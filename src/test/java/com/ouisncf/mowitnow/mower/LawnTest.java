package com.ouisncf.mowitnow.mower;

import com.ouisncf.mowitnow.setup.domain.LawnSetup;
import org.junit.Assert;
import org.junit.Test;

public class LawnTest {

    private final Lawn lawn = new Lawn(new LawnSetup(5,5));


    @Test
    public void getLawnCellByPosition() {
        LawnCell result = lawn.getLawnCellByPosition(2, 3);

        Assert.assertEquals(2,result.getX());
        Assert.assertEquals(3,result.getY());
        Assert.assertFalse(result.isBeingMowed());
    }

    @Test
    public void getLawnCellByDirection_nominal() {
        LawnCell result = lawn.getLawnCellByDirection(new LawnCell(2, 2, Boolean.FALSE), Direction.NORTH);

        Assert.assertEquals(2, result.getX());
        Assert.assertEquals(3, result.getY());
    }

    @Test
    public void getLawnCellByDirection_NOCellIndirection() {
        LawnCell result = lawn.getLawnCellByDirection(new LawnCell(0, 0, Boolean.FALSE), Direction.SOUTH);
        Assert.assertNull(result);
    }

}
