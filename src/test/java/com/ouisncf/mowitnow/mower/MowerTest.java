package com.ouisncf.mowitnow.mower;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class MowerTest {

    @Test
    public void rotateLeft() {

        Mower mower = new Mower();
        Position position = new Position();
        position.setDirection(Direction.NORTH);
        mower.setCurrentPosition(position);

        mower.rotateLeft();

        Assert.assertEquals(Direction.WEST, mower.getCurrentPosition().getDirection());
    }

    @Test
    public void rotateRight() {
        Mower mower = new Mower();
        Position position = new Position();
        position.setDirection(Direction.NORTH);
        mower.setCurrentPosition(position);

        mower.rotateRight();

        Assert.assertEquals(Direction.EST, mower.getCurrentPosition().getDirection());
    }

    @Test
    public void moveForward() {

        Mower mower = new Mower();
        Position position = new Position();
        position.setDirection(Direction.NORTH);
        mower.setCurrentPosition(position);
        LawnCell cell = new LawnCell(2,2,Boolean.TRUE);
        position.setCurrentCell(cell);

        Lawn lawn = Mockito.mock(Lawn.class);
        mower.setLawn(lawn);

        LawnCell newLAwnCell = new LawnCell(2,3,Boolean.FALSE);
        Mockito.when(lawn.getLawnCellByDirection(cell, position.getDirection())).thenReturn(newLAwnCell);

        Mockito.doNothing().when(lawn).endMowing(cell);
        Mockito.doNothing().when(lawn).startMowing(newLAwnCell);

        mower.moveForward();

        Assert.assertEquals(newLAwnCell, mower.getCurrentPosition().getCurrentCell());
        Mockito.verify(lawn).endMowing(cell);
        Mockito.verify(lawn).startMowing(newLAwnCell);

    }

}
