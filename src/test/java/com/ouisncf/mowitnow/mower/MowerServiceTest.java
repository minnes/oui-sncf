package com.ouisncf.mowitnow.mower;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class MowerServiceTest {

    @InjectMocks
    private MowerService service;

    @Test
    public void run_moveForward() {
        Mower mower = Mockito.mock(Mower.class);
        List<MowerCommand> commands = new ArrayList<>();
        commands.add(MowerCommand.MOVING_FORWARD);

        Mockito.doNothing().when(mower).moveForward();
        Mockito.doNothing().when(mower).mow();

        service.run(mower, commands);

        Mockito.verify(mower).moveForward();
        Mockito.verify(mower).mow();

    }

    @Test
    public void run_rotateLeft() {
        Mower mower = Mockito.mock(Mower.class);
        List<MowerCommand> commands = new ArrayList<>();
        commands.add(MowerCommand.TO_THE_LEFT);

        Mockito.doNothing().when(mower).rotateLeft();

        service.run(mower, commands);

        Mockito.verify(mower).rotateLeft();
    }

    @Test
    public void run_rotateRight() {
        Mower mower = Mockito.mock(Mower.class);
        List<MowerCommand> commands = new ArrayList<>();
        commands.add(MowerCommand.TO_THE_RIGHT);

        Mockito.doNothing().when(mower).rotateRight();

        service.run(mower, commands);

        Mockito.verify(mower).rotateRight();
    }


}
