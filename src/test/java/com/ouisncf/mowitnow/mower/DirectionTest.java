package com.ouisncf.mowitnow.mower;

import org.junit.Assert;
import org.junit.Test;

public class DirectionTest {

    @Test
    public void fromShortName_nominal() {
        Direction result = Direction.fromShortName("N");

        Assert.assertEquals(Direction.NORTH, result);
    }

    @Test
    public void fromShortName_notExistingDirection() {
        Direction result = Direction.fromShortName("D");

        Assert.assertNull(result);
    }


    @Test
    public void left() {
        Assert.assertEquals(Direction.WEST,Direction.left(Direction.NORTH));
        Assert.assertEquals(Direction.SOUTH,Direction.left(Direction.WEST));
        Assert.assertEquals(Direction.EST,Direction.left(Direction.SOUTH));
        Assert.assertEquals(Direction.NORTH,Direction.left(Direction.EST));
    }

    @Test
    public void right() {
        Assert.assertEquals(Direction.EST,Direction.right(Direction.NORTH));
        Assert.assertEquals(Direction.SOUTH,Direction.right(Direction.EST));
        Assert.assertEquals(Direction.WEST,Direction.right(Direction.SOUTH));
        Assert.assertEquals(Direction.NORTH,Direction.right(Direction.WEST));
    }
}
