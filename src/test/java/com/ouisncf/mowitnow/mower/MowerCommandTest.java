package com.ouisncf.mowitnow.mower;

import org.junit.Assert;
import org.junit.Test;

public class MowerCommandTest {

    @Test
    public void fromShortName_wrongValue() {
        MowerCommand result = MowerCommand.fromShortName("Z");

        Assert.assertNull(result);
    }

    @Test
    public void fromShortName_nominal() {
        MowerCommand result = MowerCommand.fromShortName("A");

        Assert.assertEquals(MowerCommand.MOVING_FORWARD, result);
    }
}
