package com.ouisncf.mowitnow.setup;

import com.ouisncf.mowitnow.setup.domain.LawnSetup;
import com.ouisncf.mowitnow.setup.parser.LawnParser;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class LawnParserTest {

    @InjectMocks
    private LawnParser lawnParser;


    @Test
    public void parse_nullInput() {
        LawnSetup result = lawnParser.parse(null);

        Assert.assertNull(result);
    }

    @Test
    public void parse_wrongPattern() {
        LawnSetup result = lawnParser.parse("2 a");

        Assert.assertNull(result);
    }


    @Test
    public void parse_nominalCase() {
        LawnSetup result = lawnParser.parse("2 4");

        Assert.assertEquals(2, result.getMaxX());
        Assert.assertEquals(4, result.getMaxY());
    }
}
