package com.ouisncf.mowitnow.setup;

import com.ouisncf.mowitnow.mower.MowerCommand;
import com.ouisncf.mowitnow.setup.domain.LawnSetup;
import com.ouisncf.mowitnow.setup.domain.MowerSetup;
import com.ouisncf.mowitnow.setup.domain.PositionSetup;
import com.ouisncf.mowitnow.setup.domain.SetupData;
import com.ouisncf.mowitnow.setup.parser.LawnParser;
import com.ouisncf.mowitnow.setup.parser.MowerCommandParser;
import com.ouisncf.mowitnow.setup.parser.MowerInitialPositionParser;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class SetupBuilderTest {

    @InjectMocks
    private SetupBuilder builder;

    @Mock
    private LawnParser lawnParser;

    @Mock
    private MowerCommandParser mowerCommandParser;

    @Mock
    private MowerInitialPositionParser mowerInitialPositionParser;

    @Test
    public void readFile_nominal() throws IOException, URISyntaxException {
        SetupData result = builder.readFile("nominalTestCase.txt");

        Assert.assertEquals("5 5", result.getLawnSetupData());
        Assert.assertEquals(1, result.getMowerSetupData().size());
        Assert.assertEquals(2, result.getMowerSetupData().get(0).size());
        Assert.assertEquals("1 2 N", result.getMowerSetupData().get(0).get(0));
        Assert.assertEquals("GAGAGAGAA", result.getMowerSetupData().get(0).get(1));
    }
    //TODO : need more test to check invalid input file

    @Test
    public void getLawnSetup_nominal() {
        SetupData setupData = new SetupData();
        setupData.setLawnSetupData("3 3");

        LawnSetup lawnSetup = new LawnSetup();
        Mockito.when(lawnParser.parse(setupData.getLawnSetupData())).thenReturn(lawnSetup);

        LawnSetup result = builder.getLawnSetup(setupData);

        Assert.assertEquals(lawnSetup, result);
        Mockito.verify(lawnParser).parse(setupData.getLawnSetupData());
    }


    @Test
    public void getMowerSetups_nominal() {
        SetupData setupData = new SetupData();
        List<List<String>> setups = new ArrayList<>();
        List<String> setup1 = new ArrayList<>();
        setup1.add("1 2 N");
        setup1.add("AGDGDA");
        setups.add(setup1);
        setupData.setMowerSetupData(setups);

        PositionSetup positionSetup = new PositionSetup();
        Mockito.when(mowerInitialPositionParser.parse(setup1.get(0))).thenReturn(positionSetup);
        List<MowerCommand> commands = new ArrayList<>();
        commands.add(MowerCommand.MOVING_FORWARD);
        Mockito.when(mowerCommandParser.parse(setup1.get(1))).thenReturn(commands);
        List<MowerSetup> result = builder.getMowerSetups(setupData);

        Assert.assertEquals(1,result.size());
        Assert.assertEquals(positionSetup, result.get(0).getInitialPosition());
        Assert.assertEquals(commands, result.get(0).getCommands());
    }




}
