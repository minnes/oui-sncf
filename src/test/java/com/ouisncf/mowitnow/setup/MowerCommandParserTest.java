package com.ouisncf.mowitnow.setup;

import com.ouisncf.mowitnow.mower.MowerCommand;
import com.ouisncf.mowitnow.setup.parser.MowerCommandParser;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class MowerCommandParserTest {

    @InjectMocks
    private MowerCommandParser parser;

    @Test
    public void parse_nullInput() {
        List<MowerCommand> result = parser.parse(null);

        Assert.assertTrue(result.isEmpty());
    }

    @Test
    public void parse_wrongInput_wrondCommand() {

        List<MowerCommand> result = parser.parse("ADGZ");

        Assert.assertNotNull(result);
        Assert.assertTrue(result.isEmpty());
    }


    @Test
    public void parse_nominal() {
        List<MowerCommand> result = parser.parse("ADGGA");

        Assert.assertEquals(5, result.size());
        Assert.assertEquals(MowerCommand.MOVING_FORWARD, result.get(0));
        Assert.assertEquals(MowerCommand.TO_THE_RIGHT, result.get(1));
        Assert.assertEquals(MowerCommand.TO_THE_LEFT, result.get(2));
        Assert.assertEquals(MowerCommand.TO_THE_LEFT, result.get(3));
        Assert.assertEquals(MowerCommand.MOVING_FORWARD, result.get(4));
    }


}
