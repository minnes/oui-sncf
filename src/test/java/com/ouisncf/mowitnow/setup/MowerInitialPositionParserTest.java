package com.ouisncf.mowitnow.setup;

import com.ouisncf.mowitnow.mower.Direction;
import com.ouisncf.mowitnow.setup.domain.PositionSetup;
import com.ouisncf.mowitnow.setup.parser.MowerInitialPositionParser;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MowerInitialPositionParserTest {

    @InjectMocks
    private MowerInitialPositionParser parser;


    @Test
    public void parse_nullInput() {
        PositionSetup result = parser.parse(null);

        Assert.assertNull(result);
    }


    @Test
    public void parse_wrongInput_missingValue() {
        PositionSetup result = parser.parse("2 2 ");

        Assert.assertNull(result);
    }


    @Test
    public void parse_wrongInput_wrondDirection() {
        PositionSetup result = parser.parse("2 2 Z");

        Assert.assertNull(result);
    }


    @Test
    public void parse_nominal() {
        PositionSetup result = parser.parse("2 4 N");

        Assert.assertEquals(2, result.getX());
        Assert.assertEquals(4, result.getY());
        Assert.assertEquals(Direction.NORTH, result.getDirection());
    }

}
