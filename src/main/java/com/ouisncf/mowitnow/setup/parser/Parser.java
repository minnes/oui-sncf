package com.ouisncf.mowitnow.setup.parser;

/**
 *
 * Parser interface
 * implemented by class that need to parse setup data
 *
 * @param <X>
 */
public interface Parser<X> {
    X parse(String toBeParsed);
}
