package com.ouisncf.mowitnow.setup.parser;

import com.ouisncf.mowitnow.mower.Direction;
import com.ouisncf.mowitnow.setup.domain.PositionSetup;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * Service class use to parse initial mower position -> (2 2 N)
 *
 */
@Service
public class MowerInitialPositionParser implements Parser<PositionSetup> {
    private final Pattern pattern = Pattern.compile("^(\\d+) (\\d+) ([N,E,W,S])$");

    @Override
    public PositionSetup parse(String toBeParsed) {

        if(StringUtils.isNotEmpty(toBeParsed) ){
            Matcher matcher = pattern.matcher(toBeParsed);
            if(matcher.matches()){
                PositionSetup position = new PositionSetup();
                position.setX(Integer.parseInt(matcher.group(1)));
                position.setY(Integer.parseInt(matcher.group(2)));
                position.setDirection(Direction.fromShortName(matcher.group(3)));

                return position;
            }
        }

        return null;

    }
}
