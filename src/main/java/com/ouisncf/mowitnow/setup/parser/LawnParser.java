package com.ouisncf.mowitnow.setup.parser;

import com.ouisncf.mowitnow.setup.domain.LawnSetup;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * Service class use to parse lawn data -> (5 5)
 *
 */
@Service
public class LawnParser implements Parser<LawnSetup>{

    private final Pattern pattern = Pattern.compile("^(\\d+) (\\d+)$");

    @Override
    public LawnSetup parse(String toBeParsed) {

        if(StringUtils.isNotEmpty(toBeParsed) ){
            Matcher matcher = pattern.matcher(toBeParsed);
            if(matcher.matches()){
                LawnSetup lawn = new LawnSetup();
                lawn.setMaxX(Integer.parseInt(matcher.group(1)));
                lawn.setMaxY(Integer.parseInt(matcher.group(2)));
                return lawn;
            }
        }

        return null;
    }
}
