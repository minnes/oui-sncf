package com.ouisncf.mowitnow.setup.parser;

import com.ouisncf.mowitnow.mower.MowerCommand;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 *
 * Service class use to parse mower commands -> (ADAGA)
 *
 */
@Service
public class MowerCommandParser  implements Parser<List<MowerCommand>>{
    private final Pattern pattern = Pattern.compile("^[A,G,D]+$");


    @Override
    public List<MowerCommand> parse(String toBeParsed) {

        if(StringUtils.isNotEmpty(toBeParsed) && pattern.matcher(toBeParsed).matches()){
            return toBeParsed.chars()
                    .mapToObj(command -> String.valueOf((char) command))
                    .map(command -> MowerCommand.fromShortName(command))
                    .collect(Collectors.toList());
        }

        return new ArrayList<>();
    }

}
