package com.ouisncf.mowitnow.setup.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Class representing datas read from input file
 *
 */
@Setter
@Getter
public class SetupData {

    private String lawnSetupData;
    private List<List<String>> mowerSetupData;

}
