package com.ouisncf.mowitnow.setup.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class representing necessary data to setup a rectangular lawn
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class LawnSetup {

    private int maxX;
    private int maxY;

}
