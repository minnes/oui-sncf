package com.ouisncf.mowitnow.setup.domain;

import com.ouisncf.mowitnow.mower.Direction;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;


/**
 * Class representing necessary data to setup the initial point where the mower will be spawn
 */
@Setter
@Getter
public class PositionSetup {
    private int x;
    private int y;
    private Direction direction;


    @Override
    public String toString() {
        return new StringBuilder().append(this.getX()).append(StringUtils.SPACE).append(this.getY()).append(StringUtils.SPACE).append(this.direction).toString();
    }
}
