package com.ouisncf.mowitnow.setup.domain;

import com.ouisncf.mowitnow.mower.MowerCommand;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Class representing necessary data to fully setup a mower
 */
@Getter
@Setter
@AllArgsConstructor
public class MowerSetup  {

    private PositionSetup initialPosition;
    private List<MowerCommand> commands;

}
