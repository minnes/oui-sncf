package com.ouisncf.mowitnow.setup;

import com.ouisncf.mowitnow.mower.MowerCommand;
import com.ouisncf.mowitnow.setup.domain.LawnSetup;
import com.ouisncf.mowitnow.setup.domain.MowerSetup;
import com.ouisncf.mowitnow.setup.domain.PositionSetup;
import com.ouisncf.mowitnow.setup.domain.SetupData;
import com.ouisncf.mowitnow.setup.parser.LawnParser;
import com.ouisncf.mowitnow.setup.parser.MowerCommandParser;
import com.ouisncf.mowitnow.setup.parser.MowerInitialPositionParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * Service class use to read file, create setup to create Lawn and mower
 *
 */
@Service
public class SetupBuilder {

    @Autowired
    private LawnParser lawnParser;


    @Autowired
    private MowerCommandParser mowerCommandParser;


    @Autowired
    private MowerInitialPositionParser mowerInitialPositionParser;


    private static final int FIRST_FILE_LINE_INDEX = 0;

    /**
     *
     * Read file from classpath, build an object representing lawn setup as a text line and a list of tuple for mower setup (initial position & commands)
     *
     * @param file path to file on classpath
     *
     * @return
     *
     * @throws IOException file can't be found
     * @throws URISyntaxException file can't be found
     */
    public SetupData readFile(String file) throws IOException, URISyntaxException {
        SetupData setupData = new SetupData();

        //Load file and retrieve it as list of text line
        Path path = Paths.get(getClass().getClassLoader().getResource(file).toURI());
        Stream<String> lines = Files.lines(path);
        List<String> textLines = lines.collect(Collectors.toList());

        //Store first line as lawn setup data
        setupData.setLawnSetupData(textLines.remove(FIRST_FILE_LINE_INDEX));

        //Store by group of 2 lines for mower setup (initial position and commands)
        final AtomicInteger counter = new AtomicInteger(0);
        setupData.setMowerSetupData(new ArrayList(textLines.stream().collect(Collectors.groupingBy(it -> counter.getAndIncrement() / 2)).values()));

        return setupData;
    }

    /**
     *
     * From text data, we build a LawnSetup that will be used to create the lawn
     *
     * @param setupData
     * @return
     */
    public LawnSetup getLawnSetup(SetupData setupData) {
        return lawnParser.parse(setupData.getLawnSetupData());
    }


    /**
     *
     * From group of two lines, we build mower setup data (eg. initial position and commands to run)
     *
     * @param setupData
     * @return
     */
    public List<MowerSetup> getMowerSetups(SetupData setupData) {
        return setupData.getMowerSetupData().stream().map(conf -> {
            PositionSetup position = mowerInitialPositionParser.parse(conf.get(0));
            List<MowerCommand> setup = mowerCommandParser.parse(conf.get(1));

            if(position != null && setup != null){
                 return new MowerSetup(position,setup);
            }
            return null;
        }).collect(Collectors.toList());
    }
}
