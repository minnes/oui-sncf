package com.ouisncf.mowitnow;

import com.ouisncf.mowitnow.mower.Lawn;
import com.ouisncf.mowitnow.mower.Mower;
import com.ouisncf.mowitnow.mower.MowerService;
import com.ouisncf.mowitnow.setup.SetupBuilder;
import com.ouisncf.mowitnow.setup.domain.LawnSetup;
import com.ouisncf.mowitnow.setup.domain.MowerSetup;
import com.ouisncf.mowitnow.setup.domain.SetupData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

@Service
public class MowerControlCenter {

    @Autowired
    private SetupBuilder setupBuilder;

    @Autowired
    private MowerService mowerService;

    public void run(String filePath) {

        try {
            SetupData setupData = setupBuilder.readFile(filePath);
            LawnSetup lawnSetup = setupBuilder.getLawnSetup(setupData);

            if(lawnSetup != null){
                Lawn lawn = new Lawn(lawnSetup);
                List<MowerSetup> mowerSetups = setupBuilder.getMowerSetups(setupData);

                mowerSetups.forEach(setup -> {
                    Mower mower = new Mower(setup, lawn);
                    mowerService.run(mower, setup.getCommands());
                });

            }else{
                System.out.println("No Lawn to mow ...");
            }
        } catch (IOException e) {
            System.out.println("can't read input file");
        } catch (URISyntaxException e) {
            System.out.println("can't read input file");
        }

    }
}
