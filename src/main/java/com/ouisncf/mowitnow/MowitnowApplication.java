package com.ouisncf.mowitnow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MowitnowApplication implements CommandLineRunner {

    public static final String NOMINAL_INPUT_TXT = "nominalInput.txt";

    public static void main(String[] args) {
        SpringApplication.run(MowitnowApplication.class, args);
    }

    @Autowired
    private MowerControlCenter mowerControlCenter;

    @Override
    public void run(String... args) {
       mowerControlCenter.run(NOMINAL_INPUT_TXT);
    }
}

