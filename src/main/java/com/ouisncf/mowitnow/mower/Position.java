package com.ouisncf.mowitnow.mower;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * Class representing a mower position : a cell and a direction the mower si heading to
 *
 */
@Setter
@Getter
public class Position {
    private LawnCell currentCell;
    private Direction direction;


    @Override
    public String toString() {
        return new StringBuilder().append(this.currentCell.toString()).append(" ").append(this.direction).toString();
    }
}
