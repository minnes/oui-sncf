package com.ouisncf.mowitnow.mower;

import com.ouisncf.mowitnow.setup.domain.MowerSetup;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class representing a mower in a lawn, at a specific position executing commands
 */
@NoArgsConstructor
@Getter
@Setter
public class Mower {

    private Position currentPosition;
    private Lawn lawn;

    public Mower(MowerSetup setup, Lawn lawn) {
        this.currentPosition = new Position();
        LawnCell currentCell = lawn.getLawnCellByPosition(setup.getInitialPosition().getX(), setup.getInitialPosition().getY());
        this.currentPosition.setCurrentCell(currentCell);
        this.currentPosition.setDirection(setup.getInitialPosition().getDirection());
        this.lawn = lawn;
    }

    public void mow() {
        System.out.println("mowing ..."+this.getCurrentPosition());
    }

    public void rotateLeft(){
        this.currentPosition.setDirection(Direction.left(this.currentPosition.getDirection()));
    }

    public void rotateRight(){
        this.currentPosition.setDirection(Direction.right(this.currentPosition.getDirection()));
    }

    public void moveForward(){
        LawnCell nextCellToGo = lawn.getLawnCellByDirection(this.currentPosition.getCurrentCell(), this.currentPosition.getDirection());
        if(nextCellToGo != null && !nextCellToGo.isBeingMowed()){
            lawn.endMowing(this.getCurrentPosition().getCurrentCell());
            this.currentPosition.setCurrentCell(nextCellToGo);
            lawn.startMowing(nextCellToGo);
        }
    }


}
