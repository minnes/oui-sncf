package com.ouisncf.mowitnow.mower;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;


/**
 *
 * Class representing geographic direction
 *
 */
@AllArgsConstructor
@Getter
public enum Direction {
    EST("E"), NORTH("N"), SOUTH("S"), WEST("W");

    private String shortName;

    /**
     * retrieve Direction from short name, if no corresponding direction, return null
     *
     * @param directionShortName "N","E", "W", "S"
     *
     * @return Direction, null if not found
     */
    public static Direction fromShortName(String directionShortName) {
        return Arrays.asList(Direction.values()).stream().filter(direction -> direction.getShortName().equalsIgnoreCase(directionShortName)).findFirst().orElse(null);
    }


    /**
     *
     * Compute 90° left direction from a given direction
     *
     * @param direction
     *
     * @return
     */
    public static Direction left(Direction direction) {
        Direction newDirection = null;
        switch (direction){
            case NORTH:newDirection =  WEST; break;
            case EST:newDirection =  NORTH; break;
            case SOUTH:newDirection =  EST; break;
            case WEST:newDirection =  SOUTH; break;
        }
        return newDirection;
    }

    /**
     *
     * Compute 90° right direction from a given direction
     *
     * @param direction
     * @return
     */
    public static Direction right(Direction direction) {
        Direction newDirection = null;
        switch (direction){
            case NORTH:newDirection =  EST; break;
            case EST:newDirection =   SOUTH; break;
            case SOUTH:newDirection =   WEST; break;
            case WEST:newDirection =   NORTH; break;
        }
        return newDirection;
    }

    @Override
    public String toString() {
        return this.getShortName();
    }
}
