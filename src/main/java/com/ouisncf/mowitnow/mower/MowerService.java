package com.ouisncf.mowitnow.mower;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MowerService {

    public void run(Mower mower, List<MowerCommand> commands) {
        commands.forEach(command -> {
            if (MowerCommand.TO_THE_LEFT == command){
                mower.rotateLeft();
            }

            if (MowerCommand.TO_THE_RIGHT == command) {
                mower.rotateRight();
            }

            if(MowerCommand.MOVING_FORWARD == command){
                mower.moveForward();
                mower.mow();
            }
        });

        System.out.println(mower.getCurrentPosition());
    }
}
