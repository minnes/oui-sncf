package com.ouisncf.mowitnow.mower;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * Class representing a cell in a lawn
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LawnCell {
    private int x;
    private int y;
    //indicate if a mower is already on the cell
    private boolean beingMowed;

    @Override
    public String toString() {
        return new StringBuilder().append(this.x).append(" ").append(this.y).toString();
    }
}
