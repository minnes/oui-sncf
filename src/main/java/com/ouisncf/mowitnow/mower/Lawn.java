package com.ouisncf.mowitnow.mower;

import com.ouisncf.mowitnow.setup.domain.LawnSetup;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Class representing a lawn and all its cells
 *
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Lawn {
    private int maxX;
    private int maxY;
    private List<LawnCell> cells = new ArrayList<>();

    public Lawn(LawnSetup lawnSetup) {
        this.maxX = lawnSetup.getMaxX();
        this.maxY = lawnSetup.getMaxY();
        this.init();
    }

    /**
     *
     * To avoid playing with indexes, we build all cells of the lawn and keep a references into a list
     *
     */
    private void init(){
        for(int x = 0 ; x <= maxX ; x ++){
            for(int y = 0 ; y <= maxY ; y ++){
                LawnCell cell = new LawnCell(x, y, Boolean.FALSE);
                cells.add(cell);
            }
        }
    }

    /**
     *
     * retrieve lawn cell for a given coordinate
     * If no cell exist with these coordinate, return null;
     *
     * @param x
     * @param y
     *
     * @return the lawn's cell corresponding or null if not found
     */
    public LawnCell getLawnCellByPosition(int x, int y){
        return cells.stream().filter(cell -> cell.getX()==x && cell.getY()==y).findFirst().orElse(null);
    }

    /**
     * Retrieve neighbour cell from a given position and direction
     *
     * @param currentCell
     * @param direction
     *
     * @return
     */
    public LawnCell getLawnCellByDirection(LawnCell currentCell, Direction direction) {
        LawnCell neighbourCell = null;
        switch(direction){
            case NORTH:neighbourCell =this.getLawnCellByPosition(currentCell.getX(), currentCell.getY()+1); break;
            case EST:neighbourCell =this.getLawnCellByPosition(currentCell.getX()+1, currentCell.getY()); break;
            case WEST:neighbourCell =this.getLawnCellByPosition(currentCell.getX()-1, currentCell.getY()); break;
            case SOUTH:neighbourCell =this.getLawnCellByPosition(currentCell.getX(), currentCell.getY()-1); break;
        }

        return neighbourCell;
    }

    /**
     *
     * change state of a given cell to indicate no mower is working on it
     *
     * @param cell cell the mower ends to work on
     */
    public void endMowing(LawnCell cell) {
        cell.setBeingMowed(Boolean.FALSE);
    }

    /**
     *
     * change state of a given cell to indicate a mower is working on it
     *
     * @param cell cell the mower starts to work on
     */
    public void startMowing(LawnCell cell) {
        cell.setBeingMowed(Boolean.TRUE);
    }
}
