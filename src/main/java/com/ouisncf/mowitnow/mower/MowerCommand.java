package com.ouisncf.mowitnow.mower;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 *
 * Class representing a mower command
 * turn to the left or right
 * MOve forward
 *
 */
@AllArgsConstructor
@Getter
public enum MowerCommand {
    TO_THE_LEFT("G"), TO_THE_RIGHT("D"), MOVING_FORWARD("A");

    private String shortName;

    public static MowerCommand fromShortName(String commandShortName) {
        return Arrays.asList(MowerCommand.values()).stream()
                .filter(command -> command.getShortName().equalsIgnoreCase(commandShortName))
                .findFirst()
                .orElse(null);
    }
}
